from core.JobBase import JobBase
from app.WMZYEntity import school_info, province_info, parma_info, majors_detail
import traceback
import json
import time
import datetime
import redis
import re
import hashlib
import requests
from dateutil import parser
import os
from queue import Queue
from bs4 import BeautifulSoup
from utils.JSHelper import get_token_from_file

DOWNLOAD_SLEEP = 0.5


# 用于续跑的标记文件
hash_key_file_path = os.path.join(os.path.dirname(__file__), "hash_key_file.txt")
if not os.path.exists(hash_key_file_path):
    with open(hash_key_file_path, "w") as f:
        f.write("")


YEAR = "2018" # 采集年份

# 一个账号的cookies
# base_cookies_test = "token=LK_mvUyg8_DDMzEpkuNagRi8FIllkw_i9XIz4Wj2zjfnogsbtAThtW2citaIUKubZIjIgyooUaIovtyNgsgJlK2u-OdLRS9EGlXJdcLX0h0=;_ga=GA1.2.473154528.1567408787; wagHohOxbLyHYrA=e4a5134d9caba3a409c857231943bcade3df7840ee8075b4fc61a; Hm_lvt_8a2f2a00c5aff9efb919ee7af23b5366=1567408849,1567473628; Hm_lpvt_8a2f2a00c5aff9efb919ee7af23b5366=1567475700; dR9BixLC1sIxD0tw="
# 另一个账号的cookies
base_cookies_test = "token=y4xqo0LPdpiVcHi-iE3tmPQWrAScjIC1uxd7D73LW91rNmyKK7s8pGIZ9bZylR-JstyYA1P448eS3SeTYpH6Oq==;_ga=GA1.2.473154528.1567408787; wagHohOxbLyHYrA=e4a5134d9caba3a409c857231943bcade3df7840ee8075b4fc61a; Hm_lvt_8a2f2a00c5aff9efb919ee7af23b5366=1567408849,1567473628; Hm_lpvt_8a2f2a00c5aff9efb919ee7af23b5366=1567475700; dR9BixLC1sIxD0tw="
# 登录cookies 两个都可以用
# token = "rlOM-TYwItDIawNTbvgeBsY-AkrGbvL-ff5Fj9VnLVXyhAIudTB_Wbq6CUIc4XejnWI95rrUaz7MwIEj01BzIq=="
token = "LK_mvUyg8_DDMzEpkuNagRi8FIllkw_i9XIz4Wj2zjfnogsbtAThtW2citaIUKubZIjIgyooUaIovtyNgsgJlK2u-OdLRS9EGlXJdcLX0h0="
# 网站端刷新cookies值


#cookies过期是只需要刷新此处
def web_get_cookies(base_cookies):
    '''获取cookies'''
    js_path = os.path.join(os.path.dirname(__file__), "wmzy.js")
    return get_token_from_file(js_path, "_$kF", base_cookies)
#
# for i in range(3):
#     a = web_get_cookies(base_cookies_test)
#     print(a)

# 哈希MD5 生成
def get_hash(data: bytes):
    m = hashlib.md5()
    m.update(data)
    return m.hexdigest()



class WMZYAction(JobBase):

    app_post_url = "https://mobile.wmzy.com/ui-api/department/getAdmissionInfo"
    app_headers = {
        "Host": "mobile.wmzy.com",
        "Connection": "keep-alive",
        "accept": "application/json",
        "Origin": "https://mobile.wmzy.com",
        "x-requested-with": "XMLHttpRequest",
        "User-Agent": "Mozilla/5.0 (Linux; Android 6.0.1; DUK-AL20 Build/MXC89K; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/44.0.2403.119 Mobile Safari/537.36 wmzyApp",
        "content-type": "application/json",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "zh-CN,en-US;q=0.8",
        "token": token,
    }

    Cookie = ""

    # 网站端headers
    headers = {
        "Host": "www.wmzy.com",
        "Connection": "keep-alive",
        "Sec-Fetch-Mode": "cors",
        "X-Requested-With": "XMLHttpRequest",
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36",
        "Accept": "*/*",
        "Sec-Fetch-Site": "same-origin",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "zh-CN,zh;q=0.9",
        "Cookie": Cookie,
    }


    check_dict = {}  # 用于续跑检测

    school_dict = {}  # 学校id_学校名称
    province_dict = {} # 省份id—省份名称
    ty_dict = {
        "li": "理",
        "wen": "文",
    }  #文理科
    batch_dict = {
        "1": "本科第一批",
        "2": "本科第二批",
        "5": "本科第三批",
        "8": "专科第一批",
    }  # 本科专科批次

    proxies = {
        # "http": "http://127.0.0.1:8888",
        # "https": "https://127.0.0.1:8888"
    }  #  代理




    def __init__(self, *args, **kwargs):
        JobBase.__init__(self, *args, **kwargs)

    # 续跑检测初始化
    def init_check_dict(self):
        with open(hash_key_file_path, "r") as f:
            infos = f.readlines()
            for line in infos:
                _key = str(line).strip()
                self.check_dict[_key] = True

    # 入口函数
    def on_run(self):
        self.init_check_dict()
        self.init_other_dict()
        self.log.info("started WMZYAction")
        # self.collect_shcool_ids_and_province()  # step1 获取学校id，province_id
        # self.generate_filter_list() # step2 获取，学校录取省份id和录取批次
        self.main_collect_score_line_info() # step3 获取专业分数线详细数据

        self.log.info("finished WMZYAction")

    # step1
    def collect_shcool_ids_and_province(self):
        '''step1 获取全部的学校id，及省份id'''
        base_url = "https://www.wmzy.com/api/school/getSchList?prov_filter=00&type_filter=0&diploma_filter=0&flag_filter=0&page={}&page_len=20&_="
        page_no = 1
        max_page = 10
        while page_no <= max_page:
            try:

                _url = base_url.format(page_no)
                headers = self.headers.copy()
                headers.pop("Cookie")
                sour = self.download_page(_url, headers=headers)
                sour = json.loads(sour)
                sour = sour.get("htmlStr")
                if page_no == 1:
                    obj_max_page = re.search('<a\s*href="#"\s*class="page\s*gb_page_last"\s*value=(.*?)>末页</a>',
                                             sour, re.S | re.I)
                    if obj_max_page:
                        max_page = obj_max_page.group(1).strip()
                        max_page = int(max_page)

                    root = BeautifulSoup(sour, "lxml")  # 存储 provinces
                    prov_filters = root.find("ul", {"id": "prov_filter"}).find_all("li")
                    for item in prov_filters:
                        try:
                            province_name = item.get_text()
                            attrs = item.attrs
                            province_id = attrs.get("data-value")
                            if province_id == '00':
                                continue

                            en = province_info()
                            en.province_id = province_id + "0000000000"
                            en.province_name = province_name
                            self.dbmanager.add_Entity_to_Session(en)
                        except:
                            pass
                self.log.info("process: %s/%s" %(page_no, max_page))
                root = BeautifulSoup(sour, "lxml")
                m_sch_list = root.find("div",{"class": "m-sch-list"}).find_all("li")

                for m_sch in m_sch_list:
                    try:
                        data_info = re.findall('<img\s*alt="(.*?)"\s*src="//storage-oss.ipin.com/school-icon/(.*?).jpg"/>', str(m_sch), re.S | re.I)
                        if data_info:
                            en = school_info()
                            data_info = data_info[0]
                            en.school_name = data_info[0]
                            en.school_id = data_info[1]
                            self.dbmanager.add_Entity_to_Session(en)
                    except:
                        pass





            except Exception as e:
                self.log.info("Exception in collect_shcools, ex: " + str(e))
            page_no += 1

    # 查询sql 初始化 school_dict = {} 和  province_dict = {}
    def init_other_dict(self):
        province_list = self.dbmanager.select_by_Session(province_info, filter={})
        for e in province_list:
            province_id = e.province_id
            province_name = e.province_name
            self.province_dict[province_id] = province_name

        sch_list = self.dbmanager.select_by_Session(school_info, filter={})
        for e in sch_list:
            school_id = e.school_id
            school_name = e.school_name
            self.school_dict[school_id] = school_name

    # step 2
    def generate_filter_list(self):
        '''step 2 ：访问每一个学校的分数线页面，获取该学校录取的省份，文理科，批次，保存数据库作为最后一步的访问参数'''
        province_list = self.dbmanager.select_by_Session(province_info, filter={})
        for e in province_list:
            province_id = e.province_id
            province_name = e.province_name
            self.school_dict[province_id] = province_name

        sch_list = self.dbmanager.select_by_Session(school_info, filter={})
        _len = len(sch_list)
        for index, e in enumerate(sch_list):
            try:
                print("process: %s/%s" %(index + 1, _len))
                school_id = e.school_id
                school_name = e.school_name
                self.school_dict[school_id] = school_name

                for province_item in province_list:
                    province_id = province_item.province_id
                    for batch_id in self.batch_dict.keys():
                        for ty in self.ty_dict.keys():
                            data = [school_id, province_id, ty, batch_id]
                            en = parma_info()
                            en.school_id = school_id
                            en.province_id = province_id
                            en.ty = ty
                            en.batch_id = batch_id
                            en.hash_key = get_hash(str(data).encode("utf-8"))
                            self.dbmanager.add_Entity_to_Session(en)
                            # filterQueue.put(data)
            except Exception as ex:
                self.log.info("Exception in generate_filter_list, ex:" + str(ex))

    # step3
    def main_collect_score_line_info(self):
        filter_list = []

        parma_infos = self.dbmanager.select_by_Session(parma_info, filter={"checked": 0})

        for en in parma_infos:
            filter_list.append({
                "batch_id": en.batch_id,
                "province_id": en.province_id,
                "school_id": en.school_id,
                "ty": en.ty
            })
        self.collect_score_line_info_from_app(filter_list)


    # app下载解析函数
    def collect_score_line_info_from_app(self, filter_list):
        for item in filter_list:

            post_data = {"province_id": item["province_id"],
                         "sch_id": item["school_id"],
                         "wenli": 1 if (item["ty"]) == "wen" else 2,
                         "batch": int(item["batch_id"]),
                         "batch_ex": "",
                         "major_category": "",
                         "enroll_year": "{}".format(YEAR),
                         "select_course": []}

            hash_key = get_hash(str(post_data).encode("utf-8"))
            if hash_key in self.check_dict:
                continue

            sour = self.download_page(self.app_post_url, data=post_data, headers=self.app_headers)
            if "success" in sour:
                print("success")
                json_info = json.loads(sour)
                data = json_info.get("data")
                count = data.get("count")
                if count == '72640025726444':
                    with open(hash_key_file_path, "a") as f:
                        f.write(hash_key + '\n')
                    continue
                else:
                    major_info_list = data.get("major_info_list")
                    for major_info in major_info_list:
                        try:
                            major_name = major_info.get("major_name")

                            score_list = major_info.get("score_list")
                            score_info = score_list[0]
                            year = self.paser_info(score_info.get("year"))
                            avg_score = self.paser_info(score_info.get("avg_score"))
                            min_score = self.paser_info(score_info.get("min_score"))
                            peopleCnt = self.paser_info(score_info.get("peopleCnt"))

                            en = majors_detail()
                            sch_id = item["school_id"]
                            school_name = self.school_dict[sch_id]
                            en.school_name = school_name
                            province_id = item["province_id"]
                            province = self.province_dict[province_id]
                            en.province = province
                            ty = item["ty"]
                            ty = self.ty_dict[ty]
                            en.ty = ty
                            en.year = year
                            batch = item["batch_id"]
                            batch = self.batch_dict[batch]
                            en.batch = batch
                            # url = "https://www.wmzy.com" + line[0]
                            majors = major_name
                            # majors_type = line[2]
                            num_of_admissions = peopleCnt
                            avg_score = avg_score
                            min_score = min_score
                            en.majors = majors
                            en.num_of_admissions = num_of_admissions
                            en.avg_score = avg_score
                            en.min_score = min_score
                            info_list = [school_name, province, ty, batch, year, majors,num_of_admissions,avg_score,min_score]
                            _hash = get_hash(str(info_list).encode("utf-8"))
                            en.hash_key = _hash
                            self.dbmanager.add_Entity_to_Session(en)
                        except Exception as ex:
                            self.log.info("Exception in parse en ,ex:" + str(ex))

                    with open(hash_key_file_path, "a") as f:
                        f.write(hash_key + '\n')

                        # print(year, avg_score, min_score, peopleCnt)

    # app端数据解析
    def paser_info(self, info):
        digit_info = {
            "25":"0", "55":"6",
            "30":"1", "60":"7",
            "35":"2", "65":"8",
            "40":"3", "70":"9",
            "45":"4", "50":"5",
                      }
        if "-1" == info or info == -1:
            return "-1"

        info = str(info).replace("726400", "")
        info = str(info).replace("1415926", "")
        info = str(info).replace("726444", "")

        # 1位
        if "3." not in info:
            return digit_info[info]
        else:
            str_s = ""
            datas = info.split("3.")
            for i in datas:
                s = digit_info[i]
                str_s += s

            return str_s






    # def collect_score_line_info(self, filter_list):
    #     base_url = "https://www.wmzy.com/api/school-score/getSchMajorScoreData?sch_id={sch_id}&diploma=7&province={province}&ty={ty}&year={year}&batch={batch}&page={page}&_="
    #
    #     for item in filter_list:
    #         page_no = 1
    #         max_page = 100
    #         success = True
    #         while page_no <= max_page and success:
    #             try:
    #                 _url = base_url.format(
    #                     sch_id=item["school_id"],
    #                     province=item["province_id"],
    #                     ty=item["ty"],
    #                     year=year,
    #                     batch=item["batch_id"],
    #                     page=page_no
    #                 )
    #                 # cooki?es = get_cookies()
    #                 sour = self.download_page(_url)
    #                 if "暂无数据，请切换年份和批次查看" in sour:
    #                     success = False
    #                     continue
    #                 elif "您的访问频率似乎有点高，先歇歇吧" in sour:
    #                     pass
    #                 else:
    #                     sour = json.loads(sour)
    #                     listHTML = sour.get("listHTML")
    #                     majorScore = listHTML.get("majorScore")
    #                     infos = re.findall('href="(.*?)"\s*target="_blank">(.*?)</a>\s*</td>\s*<td\s*width=".*?">(.*?)</td>\s*<td\s*width=".*?">(.*?)</td>\s*<td\s*width=".*?">(.*?)</td>\s*<td\s*width=".*?">(.*?)</td>\s*</tr>',
    #                               majorScore, re.S | re.I
    #                               )
    #                     for line in infos:
    #                         try:
    #                             en = majors_detail()
    #                             sch_id = item["school_id"]
    #                             school_name = self.school_dict[sch_id]
    #                             en.school_name = school_name
    #                             province_id = item["province_id"]
    #                             province = self.province_dict[province_id]
    #                             en.province = province
    #                             ty = item["ty"]
    #                             ty = self.ty_dict[ty]
    #                             en.ty = ty
    #                             en.year = year
    #                             batch = item["batch_id"]
    #                             batch = self.batch_dict[batch]
    #                             en.batch = batch
    #                             url = "https://www.wmzy.com" + line[0]
    #                             majors = line[1]
    #                             majors_type = line[2]
    #                             num_of_admissions = line[3]
    #                             avg_score = line[4]
    #                             min_score = line[5]
    #                             en.majors = majors
    #                             en.url = url
    #                             en.majors_type = majors_type
    #                             en.num_of_admissions = num_of_admissions
    #                             en.avg_score = avg_score
    #                             en.min_score = min_score
    #                             self.dbmanager.add_Entity_to_Session(en)
    #                         except Exception as ex:
    #                             self.log.info("Exception in paser data, ex: " + str(ex))
    #                             pass
    #
    #
    #
    #
    #             except Exception as ex:
    #                 self.log.info("Exception in collect_score_line_info, ex: " + str(ex))
    #
    #             page_no += 1


    def download_page(self,
                      obj_url,
                      data=None,
                      validate='',
                      validate2='',
                      maxRetry=10,
                      code="utf-8",
                      headers=None):

        sour = ''
        iRetry = 0

        if not headers:
            headers = self.headers

        while iRetry <= maxRetry:

            try:
                iRetry += 1

                if iRetry >= 2:
                    self.log.info("retry %s with download" % str(iRetry))

                if data is None:
                    response = requests.get(obj_url, headers=headers, cookies=self.cookies, proxies=self.proxies, verify=False)
                else:
                    response = requests.post(obj_url, headers=headers, cookies=self.cookies, json=data, proxies=self.proxies, verify=False)

                if not response:
                    self.log.error("did not receive any response!")
                    iRetry += 1
                    continue

                sour = response.content.decode(code)
                if "request permission error" in sour:
                    self.log.info("refersh cookies")
                    self.headers["Cookie"] = web_get_cookies()
                    # self.headers.pop("Cookie")
                    # self.cookies = get_cookies()
                    continue

                if "Token is not avaliable" in sour:
                    self.log.info("Token is not avaliable")
                    continue

                time.sleep(DOWNLOAD_SLEEP)

                if response.status_code >= 400:
                    continue

                if (validate is not None) and (validate in sour):
                    return sour

                if (validate2 is not None) and (validate2 in sour):
                    return sour

            except Exception as ex:
                    self.log.warn(str(ex))

        self.log.error("download all retry failed ")

        return sour









if __name__ == '__main__':
    WMZYAction()
