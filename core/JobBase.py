# coding: utf-8
import requests
import time
import urllib3
import datetime
import threading
import logging
from sqlalchemy import (Column, Integer, MetaData,
                         Table, create_engine, BigInteger, DATETIME, SmallInteger, VARCHAR)
from sqlalchemy.sql import func
from sqlalchemy.orm import mapper, sessionmaker, clear_mappers
from config import default_config, log_config, DOWNLOAD_SLEEP, entitybase

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)  # disable insecure  warnings


class DBUtil(object):
    Base = entitybase()
    config = default_config.get("sql_config")

    def __init__(self):
        self._init_engine_and_Session()
        self._initdb()

    def _init_engine_and_Session(self):
        self._engine = create_engine(self.config, max_overflow=5)
        self._Session = sessionmaker(bind=self._engine)()

    def _initdb(self):
        self.Base.metadata.create_all(self._engine)

    def add_Entity_to_Session(self, entity):
        '''writing in session from an entity'''
        try:
            self._Session.add(entity)
            self.save()
        except Exception as ex:
            self._Session.rollback()
            raise ex
        return True

    @property
    def Session(self):
        return self._Session

    def add_Entitys_to_Session(self, entityList):
        '''writing in session from list of EntityList'''

        if entityList:
            for entity in entityList:
                try:
                    self._Session.add(entity)
                except Exception as e:
                    self._Session.rollback()
                    raise e
        else:
            return False

        self.save()
        return True

    def select_by_Session(self, entityclass_name, filter):
        ''' entity is class , is not object()
        filter format: {id:123, name:hello}
        '''
        try:
            result = self._Session.query(entityclass_name).filter_by(**filter).all()
            return result
        except Exception as ex:
            self._Session.rollback()
            raise ex

    def select_by_sql(self, sql):
        try:
            data_query = self._Session.execute(sql)
            result = data_query.fetchall()
            return result
        except Exception as ex:
            self._Session.rollback()
            raise ex

    def update_by_sql(self, sql):
        try:
            if self._Session.execute(sql):
                return True
        except Exception as ex:
            self._Session.rollback()
            raise ex

    def update_by_Session(self):
        # step1: execute select_by_Session return result
        # step2: Direct assignment operation
        pass

    def save(self):
        '''save entitys to db'''
        try:
            self._Session.commit()
        except Exception as ex:
            self._Session.rollback()
            raise ex


class LoggerUtil(logging.Handler):
    _mapperflag = False
    Table_args = [
         Column("EventID", BigInteger, primary_key=True, autoincrement=True),
         Column("EventTime", DATETIME, nullable=False, default=func.now()),
         Column("SeverityID", SmallInteger, nullable=False),
         Column("JobID", BigInteger, nullable=False),
         Column("RunID",BigInteger, nullable=False),
         Column("PageID", Integer, default=None),
         Column("Subject", VARCHAR(100), default=''),
         Column("Details", VARCHAR(4000), default=''),
         Column("SourceFile",VARCHAR(100), default=None),
         Column("LineNum", Integer, default=None),
         Column("Class", VARCHAR(200), default=None),
         Column("Method", VARCHAR(200), default=None)]

    table_name = None
    class_name = None
    configdb_str = None
    JobID = None
    PageID = None
    RunID = None

    class LogTable(object):
        pass


    def __init__(self):
        self.config_engine = create_engine(self.configdb_str)
        self.ConfigSession = sessionmaker(bind=self.config_engine)
        self.config_session = self.ConfigSession()
        metadata = MetaData(self.config_engine)
        self.log_table = Table(self.table_name, metadata, *self.Table_args)
        metadata.create_all(self.config_engine)
        self.LogModel = metadata.tables.get(self.table_name)

        logging.Handler.__init__(self)

    def set_class_name(self, class_name):
        self.class_name = class_name


    def emit(self, record):
        if not self._mapperflag:
            mapper(self.LogTable, self.log_table)
            self._mapperflag = True
        log_model = self.LogTable()
        log_model.Class = self.class_name
        log_model.Method = str(record.funcName)
        log_model.SeverityID = record.levelno
        log_model.JobID = self.JobID
        log_model.RunID = self.RunID
        log_model.PageID = self.PageID
        args = record.args
        if args:
            log_model.Details = args[0]

        log_model.Subject = str(record.msg)

        pathname = record.pathname
        if pathname:
            if "/" in pathname:
                log_model.SourceFile = str(pathname).split('/')[-1]
            elif "\\" in pathname:
                log_model.SourceFile = str(pathname).split('\\')[-1]

        log_model.LineNum = record.lineno

        self.config_session.add(log_model)
        try:
            self.config_session.commit()
            # clear_mappers()
        except:
            self.config_session.rollback()

    # def close(self):
    #     try:
    #         self.config_session.commit()
    #     except:
    #         self.config_session.rollback()
    #     self.config_session.close()


class handlerbase(LoggerUtil):
    JobID = default_config.get("JobID")
    configdb_str = log_config

    def __init__(self, RunID, table_name):
        self.RunID = RunID
        self.table_name = table_name
        LoggerUtil.__init__(self)



class JobBase():
    RunID = 0
    MOD_BASE = 5
    project_name = default_config.get("project_name")

    headers = default_config.get("headers")

    cookies = default_config.get("cookies")

    proxies = default_config.get("proxies")

    log_by_db_flag = default_config.get("log_by_db_flag")

    rundate = datetime.datetime.now()

    def __init__(self, *args, **kwargs):
        if not self.log_by_db_flag:
            self.log = self._init_logger(db_log=False)
        else:
            self.log = self._init_logger(db_log=True)

        self.dbmanager = DBUtil()
        self.on_run()

    @property
    def _table_name(self):
        return str("event_" + self.__class__.__name__).lower()

    @property
    def _handler(self):
        handler = handlerbase(self.RunID, self._table_name)
        handler.set_class_name(self.__class__.__name__)
        return handler

    def thread_run(self, fun):
        threads = []
        for key in range(self.MOD_BASE):
            t = threading.Thread(target=fun, args=(key,), name=str(key))
            # t.setDaemon(True)
            threads.append(t)

        for t in threads:
            t.start()

        for t in threads:
            t.join()

    def _init_logger(self, db_log=True):
        logger = logging.getLogger(self.project_name)
        if db_log:
            logger.addHandler(self._handler)
        else:
            logger = logging.getLogger(self._table_name)

            console = logging.StreamHandler()
            formatter = logging.Formatter(
                ' - '.join([
                    '%(asctime)s',
                    '%(name)s',
                    '%(levelname)s',
                    '(%(module)s:%(lineno)d)',
                    '%(message)s'
                ])
            )
            console.setFormatter(formatter)
            logger.addHandler(console)
            logger.setLevel(logging.DEBUG)

        logger.setLevel(logging.DEBUG)
        return logger

    def on_run(self):
        pass


    def download_page(self,
                      obj_url,
                      data=None,
                      validate='',
                      validate2='',
                      maxRetry=10,
                      code="utf-8",
                      headers=None):

        sour = ''
        iRetry = 0

        if not headers:
            headers = self.headers

        while iRetry <= maxRetry:

            try:
                iRetry += 1

                if iRetry >= 2:
                    self.log.info("retry %s with download" % str(iRetry))

                if data is None:
                    response = requests.get(obj_url, headers=headers, cookies=self.cookies, proxies=self.proxies, verify=False)
                else:
                    response = requests.post(obj_url, headers=headers, cookies=self.cookies, data=data, proxies=self.proxies, verify=False)

                if not response:
                    self.log.error("did not receive any response!")
                    iRetry += 1
                    continue

                sour = response.content.decode(code)
                time.sleep(DOWNLOAD_SLEEP)

                if response.status_code >= 400:
                    continue

                if (validate is not None) and (validate in sour):
                    return sour

                if (validate2 is not None) and (validate2 in sour):
                    return sour

            except Exception as ex:
                    self.log.warn(str(ex))

        self.log.error("download all retry failed ")

        return sour







