from config import entitybase
from sqlalchemy import (Column, Integer, MetaData,BigInteger, SmallInteger,
                        VARCHAR , Integer, DECIMAL, DATETIME, DATE, ForeignKey, Boolean)

from sqlalchemy.dialects.mysql import LONGTEXT





class school_info(entitybase):
    __tablename__ = "wmzy_school_info"
    school_id = Column(VARCHAR(200), primary_key=True)
    school_name = Column(VARCHAR(200))
    checked = Column(Integer, default=0)
    __table_args__ = {
        'mysql_charset': 'utf8'
    }




class province_info(entitybase):
    __tablename__ = "wmzy_province_info"
    province_id = Column(VARCHAR(50), primary_key=True)
    province_name = Column(VARCHAR(200))
    checked = Column(Integer, default=0)
    __table_args__ = {
        'mysql_charset': 'utf8'
    }


class parma_info(entitybase):
    __tablename__ = "wmzy_parma_info"
    id = Column(Integer, primary_key=True, autoincrement=True)
    province_id = Column(VARCHAR(50))
    school_id = Column(VARCHAR(50))
    ty = Column(VARCHAR(50))
    batch_id = Column(VARCHAR(50))
    checked = Column(Integer, default=0)
    hash_key = Column(VARCHAR(50), unique=True)
    __table_args__ = {
        'mysql_charset': 'utf8'
    }


class majors_detail(entitybase):
    __tablename__ = "wmzy_majors_detail"
    id = Column(Integer, primary_key=True, autoincrement=True)
    school_name = Column(VARCHAR(100))
    province = Column(VARCHAR(100))
    ty = Column(VARCHAR(10))
    year = Column(VARCHAR(10))
    batch = Column(VARCHAR(10))
    majors = Column(VARCHAR(500))
    num_of_admissions = Column(VARCHAR(10))
    avg_score = Column(VARCHAR(10))
    min_score = Column(VARCHAR(10))
    hash_key = Column(VARCHAR(50), unique=True)
    __table_args__ = {
        'mysql_charset': 'utf8'
    }





